package service;


import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		 
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='data'>" +
				"Name: <input type='text' name='name' /> <br />" +
				"Surname: <input type='text' name='surname' /> <br />" +
				"Email: <input type='email' name='email' /> <br />" +
				"Address Type<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown'>Dropdown Example <span class='caret'></span></button> <ul class='dropdown-menu'> <li><a href='#'>HTML</a></li>   <li><a href='#'>CSS</a></li> <li><a href='#'>JavaScript</a></li> </ul> <br/>" +
				"Employer: <input type='text' name='employer' /> <br />" +
				"<input type='checkbox' name='info' value='Work advertisement'>Work advertisement<br />" +
				"<input type='checkbox' name='info' value='Study advertisement'>Study advertisement<br />" +
				"<input type='checkbox' name='info' value='Facebook'>Facebook<br />" +
				"<input type='checkbox' name='info' value='Friends'>Friends<br />" +
				"What do you do?<br /> <textarea rows='4' cols='50' name='Hobby'> </textarea><br />" +
				"<input type='submit' value=' OK ' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}

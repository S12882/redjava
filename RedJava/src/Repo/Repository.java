package Repo;

import java.util.*;
import Client.Profile;

public class Repository implements iRepository<Profile> {
	
	private static List<Profile> profiles = new ArrayList<Profile>();
	private int currentId=1;
	
	
	public void add(Profile entity) {
		entity.setId(currentId);
		profiles.add(entity);
		currentId++;
		
	}

	
	public void delete(Profile entity) {
		profiles.remove(entity);
	}

	
	public Profile get(int id) {
		for(Profile current : profiles){
			if(current.getId()==id) return current;
		}
		return null;
	}

	
	public List<Profile> getAll() {
		return profiles;
	}

}

package Repo;

import Client.Profile;

public interface iRepositoryCatalog {
	
	public iRepository<Profile> profiles();

}

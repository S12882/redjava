<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="bootstrap.html" %>
<html>
<head>
    <title><%  %></title>
</head>
<body>
<%@include file="navigation.html"%>

<div class="container">
    <div class="row">
        <label>Nazwa profilu: </label>
    </div>
    <div class="row">
        <div class="col-md-4">
            <img src="" class="img-rounded" alt="Profile Photo" width="304" height="236">
        </div>
        <div class="col-md-8">
            <label for="about">O mnie:</label> <br/>
            <textarea rows="9" cols="100" readonly id="about"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="col-md-4">
                <label>Wiek: </label> <br/>
                <label>Wzrost: </label> <br/>
                <label>Kolor oczu: </label> <br/>
                <label>Kolor wlosow: </label> <br/>
                <label>Wyksztalcenie: </label> <br/>
            </div>
            <div class="col-md-4">
                <label for="who">Kogo szukam:</label> <br/>
                <textarea rows="5" cols="57" readonly id="who"></textarea>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <button type="submit" value="Edytuj" class="btn btn-default">Edutuj</button>
        </div>
    </div>
</div>
</body>
</html>
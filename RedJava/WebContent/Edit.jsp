<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="bootstrap.html" %>
<html>
<head>
    <title><%  %></title>
</head>
<body>
<%@include file="navigation.html"%>

<div class="container">
   <form role="form" method="post" action="#">
       <div class="col-md-6">
           <div class="form-group">
               <label class="control-label col-sm-2" for="photo">Zdjecie: </label>
               <div class="col-md-6">
                   <input type="file" id="photo" name="photo" class="form-control" placeholder="Lokalizacja zdjęcia" required/>
               </div>
           </div>
           <br/><br/>
           <div class="form-group">
               <label class="control-label col-sm-2" for="age">Wiek: </label>
               <div class="col-md-6">
                   <input type="number" id="age" name="age" class="form-control" min="18" max="120" required/>
               </div>
           </div>
           <br/><br/>
           <div class="form-group">
               <label class="control-label col-sm-2" for="height">Wzrost: </label>
               <div class="col-md-6">
                   <input type="number" id="height" name="height" class="form-control" min="50" max="250" required/>
               </div>
           </div>
           <br/><br/>
           <div class="form-group">
               <label class="control-label col-sm-2" for="eyeColor">Kolor oczu: </label>
               <div class="col-md-6">
                   <select id="eyeColor" name="eyeColor" class="form-control" required>
                   <option value="Niebieskie">Niebieskie</option>
                       <option value="Zielone">Zielone</option>
                       <option value="Brazowe">Brazowe</option>
                       <option value="Brak">Brak</option>
                   </select>
               </div>
           </div>
           <br/> <br/>
           <div class="form-group">
               <label class="control-label col-sm-2" for="hairColor">Kolor wlosów:</label>
               <div class="col-md-6">
                   <input type="text" id="hairColor" name="hairColor" class="form-control"/>
               </div>
           </div>
       </div>
       <div class="col-md-6">
           <div class="row">
           <div class="col-md-4">
               <img src="" class="img-rounded" alt="Profile Photo" width="304" height="236">
           </div>
           </div>
           <div class="form-group col-md-*">
               <label for="about">O mnie:</label> <br/>
               <textarea rows="9" cols="100" id="about"></textarea>
           </div>
       </div>
       <div class="form-group">
           <div class="col-sm-offset-2 col-sm-10">
               <button type="submit" class="btn btn-success">Zapisz</button>
           </div>
       </div>
   </form>
</div>
</body>
</html>